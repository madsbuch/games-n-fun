import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: mads
 * Date: 2/26/13
 * Time: 4:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class Driver {

	public static void main(String args[]){
		ArrayList<ArrayList<String>> out = new ArrayList<ArrayList<String>>();
		for(int u=0;u<100;u++){
			out.add(new ArrayList<String>());
			Environment v = new Environment(2000, .01 * (double) u, 20, 0, 40);

			for(int i=0;i<50;i++){
				//System.out.println("round: " + i);
				v.doGeneration();
				out.get(u).add(v.toString());
			}
		}

		for(int u=0;u<50;u++){
			for(int i=0;i<100;i++){
				System.out.print(out.get(i).get(u) + ", ");
			}
			System.out.println();
		}
	}
}
