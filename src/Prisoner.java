/**
 * Created with IntelliJ IDEA.
 * User: mads
 * Date: 2/26/13
 * Time: 4:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class Prisoner implements Agent {

	public static final int DEFECTOR = 0;
	public static final int COOPERATOR = 1;

	private int type;
	private double cost;
	private double fitness;

	public Prisoner(int type, double generationCost){
		this.type = type;
		this.cost = generationCost;
	}

	public void play(){
		this.fitness -= this.cost;
	}

	public int getType(){
		return type;
	}

	public double getFitness(){
		return this.fitness;
	}
	public void setFitness(double f){
		fitness = f;
	}

	public void reward(double amount){
		this.fitness += amount;
	}


}
