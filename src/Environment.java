import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: mads
 * Date: 2/26/13
 * Time: 4:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class Environment {

	int size;
	double distribution = 0.1;
	double mortality = 0.1;
	int coopCost;
	int defCost;
	int reward;

	ArrayList<Agent> agentList = new ArrayList<Agent>();
	ArrayList<Agent> tmpList = new ArrayList<Agent>();


	public Environment(int size, double distribution, int coopCost, int defCost, int reward){
		//set variables
		this.reward = reward;
		this.size = size;
		this.distribution = distribution;
		this.coopCost =  coopCost;
		this.defCost = defCost;

		for ( int i = 0 ; i < size ; i++){
			this.agentList.add(getAgent(distribution));
		}

	}

	/**
	 * main function, executes an generation
	 */
	public void doGeneration(){
		//interaction
		this.interact();

		//motality (dræb x procent)
		this.mortality();

		//reproduction
		this.reproduction();

		for (Agent a : agentList){
			Prisoner p = (Prisoner) a;
			p.setFitness(0);
		}

	}

	public void interact(){
		interact(6);
	}
	/**
	 *
	 */
	public void interact(int n){
		int num = agentList.size() / n;
		for (int u = 0; u < num ; u++){
			ArrayList<Prisoner> l = new ArrayList<Prisoner>();
			//get Agents
			for(int i=0;i<n;i++){
				Prisoner a = (Prisoner) getRandomAgent();
				l.add(a);
				a.play();
			}

			//are most cooperators
			int cooperators = 0;
			for(Prisoner p : l){
				if(p.getType() == Prisoner.COOPERATOR)
					cooperators++;
			}

			//reward them
			if(cooperators > n/2){
				for(Prisoner p : l){
					p.reward(reward);
				}
			}
		}
	}

	/**
	 * kill som percentage
	 */
	public void mortality(){
		int size = agentList.size();
		int num = (int) (size * mortality);
		for(int i = 0; i<num ; i++){
			agentList.remove((int) ((size * Math.random())) );
			size--;
		}
	}

	/**
	 *
	 */
	public void reproduction(){
		int missing = size - agentList.size();

		//calculate averages
		double coopFitness=0;
		double defFitness=0;
		int coopNum=0;
		int defNum=0;

		for(Agent a : agentList){
			Prisoner p = (Prisoner) a;

			if(p.getType() == Prisoner.COOPERATOR){
				coopNum++;
				coopFitness += p.getFitness();
			}
			else{
				defNum++;
				defFitness += p.getFitness();
			}
		}

		double avgDef = defFitness/(double) defNum;
		double avgCoop = coopFitness/(double) coopNum;
		double size = (double) agentList.size();

		double defStuff = avgDef * (defNum / size);
		double coopStuff = avgCoop * (coopNum / size);

		double fecondity = coopStuff / (coopStuff + defStuff);

		//System.out.println("fecondity: "+ fecondity + " cooperators: " + coopNum + " defectors: " + defNum);
		//System.out.println(defNum);

		for (int i = 0 ; i<missing ; i++){
			agentList.add(getAgent(fecondity));
		}
	}

	/**
	 *
	 * @return
	 */
	public Agent getRandomAgent(){

		return agentList.get((int) (agentList.size() * Math.random()));
		/*agentList.remove(0);
		tmpList.add(a);
		return a;*/
	}

	/**
	 * initialize new Agent
	 *
	 * @param distribution
	 * @return
	 */
	private Agent getAgent(double distribution){
		int side = Math.random() > distribution ? Prisoner.COOPERATOR : Prisoner.DEFECTOR;
		return new Prisoner(side, side == Prisoner.COOPERATOR ? coopCost : defCost);
	}

	public String toString(){
		int coopNum=0;
		int defNum=0;
		for(Agent a : agentList){
			Prisoner p = (Prisoner) a;

			if(p.getType() == Prisoner.COOPERATOR){
				coopNum++;
			}
			else{
				defNum++;
			}
		}

		int size = agentList.size();

		return "" + (double) coopNum / (double) size;


	}

}
